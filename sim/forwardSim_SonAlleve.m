% parameters
f = 1.1e6; % Hz, US frequency
omega = 2*pi*f; % rad/sec, US frequency
c = 1448; % m/s, US speed in water
lambda = c/f; % m, US wavelength in water
k = 2*pi/lambda; % wavenumber
n0 = 1.3325; % base index of refraction
Zd = 0.1; % m, distance from center of US focus to background
dn0dp = (1.3475-1.3325)/((1100-1)*100000); % dindex/dPascal, from Waxler paper, read from Figure 2 at 24.8 deg C
pmax = 15e6; % Pascal; peak pressure
%Texp = 1/30; % exposure time
%dt = 1/f/19.9999; % 20 samples per US period
Nt = 1000; % time samples
%t = 0:dt:Texp - dt; % time vector
dt = 1/f/Nt;
t = 0:dt:1/f - dt;

% load a field profile; define spatial vector
load H101_Sim_1p1MHz; 
pmap_single = pmap_single./max(pmap_single(:))*pmax;
p = squeeze(pmap_single(:,:,:));
Nx = size(p,1);
dx = 0.064/Nx; % resolution in m, 0.064 comes from Charles' email
x = 0:dx:(Nx-1)*dx; % spatial coordinates

% numerically integrate the pattern through z
p = sum(p,3)*dx;
%p = p(:,:,97)/Nx;

% Overal loop (more general than below since any background image can be used):
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% For each spatial location:
%   Calculate displacements
%   For each line position/output image:
%       Add displaced pixels into image
imgs = calcImg(p,f,t,Zd/dx*(k*dn0dp)/n0);

% build a dictionary of signals at different amplitudes
dp = 10; % Pa, step size of dictionary
[D,plist] = build_dictionary(max(abs(p(:))),dp,Zd/dx*(k*dn0dp)/n0,omega,t);

% reconstruct the original pressure field
pRec = recon_p(imgs,D,plist);


