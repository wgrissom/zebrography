function p = recon_p(imgs,D,plist)

[Nx,Ny,Nimg] = size(imgs);
[~,Nd] = size(D); % # of pressures, # of points in each signature

% Loop through the images, find the best p in each case
p = zeros(Nx,Ny); % to store the recon'd pressure field
for ii = 1:Nimg
    
    % calculate comparison indices, considering edges
    imgCompInds = max(ii-floor(Nd/2),1):min(ii+floor(Nd/2),Nx);
    dCompInds = imgCompInds-ii+ceil(Nd/2);
    
    Dt = D(:,dCompInds); % extract dictionary columns if near edges
    
    tmp = sqz(imgs(imgCompInds,:,ii)); % extract image columns to evaluate
    
    % find the best match for each row
    for jj = 1:Ny
        innProd = (Dt*tmp(:,jj));
        p(ii,jj) = plist(find(innProd == max(innProd),1,'first'));
    end
    
end