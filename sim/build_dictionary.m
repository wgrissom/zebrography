function [D,plist] = build_dictionary(pmax,dp,pToPixDisp,omega,t)

plist = dp:dp:pmax; % Pa, range of pressures the dictionary is defined over
epsilonPixMax = pToPixDisp*pmax;
Nd = ceil(epsilonPixMax*2*1.5); % total width, plus 50%
if ~rem(Nd,2)
    Nd = Nd + 1;
end
s = sin(omega*t);
epsPixDictMat = pToPixDisp*(plist(:)*s(:).');

% average displacements
D = zeros(length(plist),Nd);
for jj = 1:length(t)

    lowerDispInds = floor(epsPixDictMat(:,jj))+ceil(Nd/2);
    upperDispInds = ceil(epsPixDictMat(:,jj))+ceil(Nd/2);
    
    inds = sub2ind(size(D),1:length(plist),lowerDispInds');
    D(inds) = D(inds) + ...
        (ceil(epsPixDictMat(:,jj)) - epsPixDictMat(:,jj))';
    inds = sub2ind(size(D),1:length(plist),upperDispInds');
    D(inds) = D(inds) + ...
        (epsPixDictMat(:,jj) - floor(epsPixDictMat(:,jj)))';
    
end

% normalize each entry
D = D./repmat(sum(D.^2,2),[1 Nd]);
