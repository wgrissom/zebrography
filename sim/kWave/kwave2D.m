% Create a spherically focused single element transducer and simulate
% sinusoidal output

addpath k-wave-toolbox-version-1.1.1/k-Wave/

clear all;
close all;
% Create the computational grid
%for R_factor=1.2:0.2:4

% =========================================================================
% SIMULATIONs
% =========================================================================

%% grid and medium characteristics

% create the computational grid
Nx = 256*2;           % number of grid points in the x (row) direction
Ny = 256*2;           % number of grid points in the y (column) direction
%Nz = 128;
dx = 0.1e-3;    	% grid point spacing in the x direction [m]
dy = dx;            % grid point spacing in the y direction [m]
%dz = dx;
kgrid = makeGrid(Nx, dx, Ny, dy);

% define the properties of the propagation medium
medium.sound_speed = 1484*ones(Nx,Ny);	% [m/s], orginally 1480
medium.density = 1000; % [kg/m^3]
medium.BonA = 5.2; %%%%water%%%%
medium.alpha_power = 2;
alpha_0 = 0.0022; % absorption coefficient [dB/(MHz^2 cm)],Lockwood et al., 1994,orginally 0.25
medium.alpha_coeff = alpha_0;
medium.alpha_mode = 'no_dispersion';

% create the time array
[kgrid.t_array, dt] = makeTime(kgrid, medium.sound_speed);
Nt = length(kgrid.t_array);

%% sources

% create a concave source 
% a drawing!!!!!!! CHECK IF IT CORRESPONDS TO H101
%R_factor = 0.95;
R=32e-03; % Radius of spherical cap in [m]
a_trans=sqrt(32^2-31.6^2)*10^-3; % radius of exit plane [m]
h = (63.2 - 51.74)*1e-03;%(2*R-sqrt((-2*R)^2-4*1*a_trans^2))/2*1e3 % solve for height [m]
% alpha = acos(a/R);
% beta = pi-(alpha+pi/2);
%my_angle = beta;

%height = round(9.7e-3/dx);
%height = round(0.03e-3/dx*25.4);

%my_angle = 30;
%my_angle = 17;
%my_angle = 50;
%ss = makeCircle(Nx,Ny,0,0,R/dx,beta*2,1);
%size(ss)*dx/25.4e-3

transducer_offset = 40;
ss = makeArc(Nx,Ny,round(R/dx),round(a_trans/dx),transducer_offset);

% add it to a mask of the correct size
source.p_mask = zeros(Nx, Ny);
[a,b] = size(ss);
sphere_offset = 0;
source.p_mask((1:a),(1:b)) = ss;
%source.p_mask = imrotate(source.p_mask,-(90-(beta*180/pi)),'crop')
%source.p_mask = imrotate(source.p_mask,-(90-my_angle/2),'crop')

xaxis = (-Nx/2*dx:dx:dx*(Nx/2-1))*1e3;
yaxis = (-Ny/2*dy:dy:dy*(Ny/2-1))*1e3;
imagesc(source.p_mask,'XData',yaxis,'YData',xaxis);axis equal;

%%
% define a time varying sinusoidal source
source_freq = 1.1e6;       % [Hz]
source_mag = 3e6;           % [Pa]
source.p = source_mag*sin(2*pi*source_freq*kgrid.t_array);

% filter the source to remove any high frequencies not supported by the grid
%%%%source.p = filterTimeSeries(kgrid, medium, source.p);

%% create sensor

% create a sensor mask covering the entire computational domain
sensor.mask = ones(Nx, Ny);

%sensor.mask = zeros(Nx, Ny);
%sensor.mask((290-40):(290+40),(256-20):(256+20))=1;

% set the record mode capture the final wave-field and the statistics at
% each sensor point 
%sensor.record = {'p'};
sensor.record = {'p'}; %%%% CHANGE: NEED ONE ENTIRE CYCLE %%%%%


%% run simulation



% run the first simulation
%source.p_mask = source1;
%input_args = {'PMLSize', 10, 'DataCast', 'gpuArray-single', 'PlotSim', true};
input_args = {'PMLSize', 10, 'DataCast', 'single', 'PlotSim', true};
sensor_data1 = kspaceFirstOrder2D(kgrid, medium, source, sensor, input_args{:});


% reshape the sensor data
%%%%%sensor_data1.p_max = reshape(sensor_data1.p_max, Nx, Ny);
%sensor_data1.p_rms = reshape(sensor_data1.p_rms, Nx, Ny);

%% show result
figure;
xaxis = (-Nx/2*dx:dx:dx*(Nx/2-1))*1e3;
yaxis = (-Ny/2*dy:dy:dy*(Ny/2-1))*1e3;
pmax = sensor_data1.p_max;

imagesc(sensor_data1.p_max,'XData',yaxis,'YData',xaxis);axis equal;
[max_val, max_i] = max(pmax(:));
[max_m,max_n] = ind2sub(size(pmax),max_i);
focal_distance = (max_m-transducer_offset)*dx

%% plot single lines of beam profile
[m, n] = find(sensor_data1.p_max==max(sensor_data1.p_max(:)));

figure;
% lateral profile
subplot(121);
lateral_profile = sensor_data1.p_max(m,:);
plot(yaxis,lateral_profile);

[max_val, max_i]=max(lateral_profile);
lateral_distance = 8e-3/dx; % [pixels]
lateral_neighborhood = lateral_profile(max_i-round(lateral_distance):end);
yaxis_neighborhood = yaxis(max_i-round(lateral_distance):end);

[max_val_n, max_i_n] = max(lateral_neighborhood);

plot(yaxis_neighborhood,lateral_neighborhood)

half_max=0.5*max_val;
eps=max_val*0.1;
half_max_i = intersect(find(lateral_neighborhood<half_max+eps),find(lateral_neighborhood>=half_max-eps));

half_max_lower_i = round(median(half_max_i(find(half_max_i<max_i_n))));
if isnan(half_max_lower_i)
    half_max_lower_i=1;
end;
half_max_upper_i = round(median(half_max_i(find(half_max_i>=max_i_n))));
if isnan(half_max_upper_i)
    half_max_lower_i=length(axial_neighborhood);
end;

hold on;
stem(yaxis_neighborhood(half_max_lower_i),lateral_neighborhood(half_max_lower_i))
stem(yaxis_neighborhood(half_max_upper_i),lateral_neighborhood(half_max_upper_i))
FWHM_lateral_pressure = xaxis(half_max_upper_i)-xaxis(half_max_lower_i)


% half maximum
%     max_val=max(lateral_profile);
%     half_max=0.5*max_val;
%     eps=max_val*0.25;
%     half_max_i = intersect(find(lateral_profile<half_max+eps),find(lateral_profile>=half_max-eps))
%     half_max_lower_i = round(median(half_max_i(1:end/2)));
%     half_max_upper_i = round(median(half_max_i(end/2:end)));
% 
%     hold on;
%     stem(yaxis(half_max_lower_i),lateral_profile(half_max_lower_i))
%     stem(yaxis(half_max_upper_i),lateral_profile(half_max_upper_i))
%     FWHM_lateral_pressure = yaxis(half_max_upper_i)-yaxis(half_max_lower_i);

%% 

subplot(122);
axial_profile = sensor_data1.p_max(:,n);

plot(xaxis,axial_profile);

[max_val max_i]=max(axial_profile);
distance_back = 10e-3/dx; % [pixels]

axial_neighborhood = axial_profile(max_i-round(distance_back):end);
xaxis_neighborhood = xaxis(max_i-round(distance_back):end);

[max_val_n max_i_n] = max(axial_neighborhood);

%plot(xaxis_neighborhood,axial_neighborhood)

half_max=0.5*max_val;
eps=max_val*0.01;
half_max_i = intersect(find(axial_neighborhood<half_max+eps),find(axial_neighborhood>=half_max-eps));

half_max_lower_i = round(median(half_max_i(find(half_max_i<max_i_n))));
if isnan(half_max_lower_i)
    half_max_lower_i=1;
end;

half_max_upper_i = round(median(half_max_i(find(half_max_i>=max_i_n))));
if isnan(half_max_upper_i)
    half_max_lower_i=length(axial_neighborhood);
end;

hold on;
stem(xaxis_neighborhood(half_max_lower_i),axial_neighborhood(half_max_lower_i))
stem(xaxis_neighborhood(half_max_upper_i),axial_neighborhood(half_max_upper_i))
FWHM_axial_pressure = xaxis(half_max_upper_i)-xaxis(half_max_lower_i)
distance_to_first_HM=abs((-Nx/2+transducer_offset)*dx*1e3-xaxis_neighborhood(half_max_lower_i));
distance_to_second_HM=abs((-Nx/2+transducer_offset)*dx*1e3-xaxis_neighborhood(half_max_upper_i));


%% Display pressures
sensor_data1.p = reshape(sensor_data1.p, Nx, Ny, Nt);
for t=1:Nt
    imagesc(sensor_data1.p(:,:,t));
    pause(0.001);
end

%%

fname = sprintf('non_linear_simulation2D_R%2.3f_a%2.3f.mat',R*100,a_trans*100);
save(fname,'sensor_data1','-v7.3')

%end;

