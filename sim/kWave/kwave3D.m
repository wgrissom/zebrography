% Create a spherically focused single element transducer and simulate
% sinusoidal output

addpath k-wave-toolbox-version-1.1.1/k-Wave/

clear all;
close all;
% Create the computational grid
%for R_factor=1.2:0.2:4

% =========================================================================
% SIMULATIONs
% =========================================================================

%% grid and medium characteristics

% create the computational grid
Nx = 256*2;           % number of grid points in the x (row) direction
Ny = 256*2;           % number of grid points in the y (column) direction
Nz = 128;
dx = 0.1e-3;    	% grid point spacing in the x direction [m]
dy = dx;            % grid point spacing in the y direction [m]
dz = dx;
kgrid = makeGrid(Nx, dx, Ny, dy, Nz, dz);

% define the properties of the propagation medium
medium.sound_speed = 1484*ones(Nx,Ny,Nz);	% [m/s], orginally 1480
medium.density = 1000; % [kg/m^3]
medium.BonA = 5.2; %%%%water%%%%
medium.alpha_power = 2;
alpha_0 = 0.0022; % absorption coefficient [dB/(MHz^2 cm)],Lockwood et al., 1994,orginally 0.25
medium.alpha_coeff = alpha_0;
medium.alpha_mode = 'no_dispersion';

% create the time array
[kgrid.t_array, dt] = makeTime(kgrid, medium.sound_speed);
Nt = length(kgrid.t_array);

%% sources

% first define transduce geometry and place elemental sources in the kgrid
transducer_radius_m = (64e-03); % WAG: 6.4 cm - distance from 
transducer_height_m = (63.2 - 51.74)*1e-03; % WAG: How does this relate to transducer shape??

transducer_radius_pix = round( transducer_radius_m / dz );
transducer_height_pix = round( transducer_height_m / dx );

disp 'makeSphericalSection'
sphere = makeSphericalSection( transducer_radius_pix, transducer_height_pix, [], true ); % WAG: what is this doing?

[sx, sy, sz] = size(sphere);

source.p_mask = zeros(Nx,Ny,Nz);

%the transducer centroid touches x=0, and is centered in the y & z
%grid
ixstart = 1;
ixend = ixstart + sx-1;

iystart = floor(Ny/2 - sy/2);
iyend = iystart + sy-1;

izstart = floor(Nz/2 - sz/2);
izend = izstart + sz-1;

%update the source mask with the spherical section
source.p_mask( ixstart:ixend, iystart:iyend, izstart:izend ) = sphere; 

%%
% define a time varying sinusoidal source
source_freq = 1.1e6;       % [Hz]
source_mag = 3e6;           % [Pa]
source.p = source_mag*sin(2*pi*source_freq*kgrid.t_array);

% filter the source to remove any high frequencies not supported by the grid
disp 'filterTimeSeries'
source.p = filterTimeSeries(kgrid, medium, source.p);

%% create sensor

% create a sensor mask covering the entire computational domain
sensor.mask = ones(Nx, Ny, Nz);

%sensor.mask = zeros(Nx, Ny);
%sensor.mask((290-40):(290+40),(256-20):(256+20))=1;

% set the record mode capture the final wave-field and the statistics at
% each sensor point 
%sensor.record = {'p'};
sensor.record = {'p'}; %%%% CHANGE: NEED ONE ENTIRE CYCLE %%%%%


%% run simulation
% run the first simulation
%source.p_mask = source1;
%input_args = {'PMLSize', 10, 'DataCast', 'gpuArray-single', 'PlotSim', true}; % is this how we get GPU?
input_args = {'PMLSize', 10, 'DataCast', 'single', 'PlotSim', true};
disp 'kspaceFirstOrder3D'
sensor_data1 = kspaceFirstOrder3D(kgrid, medium, source, sensor, input_args{:});

% reshape the sensor data
%%%%%sensor_data1.p_max = reshape(sensor_data1.p_max, Nx, Ny);
%sensor_data1.p_rms = reshape(sensor_data1.p_rms, Nx, Ny);

%% Save data
fname = sprintf('Simulation_3D_R%2.3f_a%2.3f.mat',R*100,a_trans*100);
save(fname,'sensor_data1','-v7.3')
