function img = calcImg(p,f,t,alpha)

[Nx,Ny,~] = size(p);

% Overal loop (more general than below since any background image can be used):
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% For each spatial location:
%   Calculate displacements
%   For each line position/output image:
%       Add displaced pixels into image
img = zeros(Nx,Ny,Nx); % 3rd dim: line position
s = sin(2*pi*f*t);
parfor ii = 1:Nx % Loop over line positions
    
    % calculate sinusoid
    %s = sin(2*pi*f*t);% - k*x(ii)); % k doesnt matter here since we integrate over so many cycles
    %c = cos(omega*t);% - k*x(ii));
    
    tmp = zeros(Nx,Ny); 
    %for kk = 1:Nx % Loop over vertical positions
        
        % calculate displacements
        epsilonPix = (alpha*p(ii,:).')*s(:).'; % displacements - was a dt here - why?
        %epsilon = epsilon + 1/n0*c*Zd*(p(ii+1,kk)-p(ii-1,kk))/(2*dx)*dn0dp;
        %epsilonPix = Zd*epsilon/dx; % displacement in pixels
        
        % average displacements
        for jj = 1:length(t)
            %epsilonPix = epsilon(jj)/dx; % displacement in pixels
            lowerDispInds = floor(epsilonPix(:,jj))+ii;
            upperDispInds = ceil(epsilonPix(:,jj))+ii;
            OKinds = find(floor(epsilonPix(:,jj))+ii > 0 & ...
                ceil(epsilonPix(:,jj))+ii < Ny+1);
            lowerDispInds = lowerDispInds(OKinds);
            upperDispInds = upperDispInds(OKinds);
            
            inds = sub2ind([Nx,Ny],lowerDispInds,OKinds);
            tmp(inds) = tmp(inds) + ...
                (ceil(epsilonPix(OKinds,jj)) - epsilonPix(OKinds,jj));
            inds = sub2ind([Nx,Ny],upperDispInds,OKinds);
            tmp(inds) = tmp(inds) + ...
                (epsilonPix(OKinds,jj) - floor(epsilonPix(OKinds,jj)));
            
%             if floor(epsilonPix(jj))+ii > 0 && ceil(epsilonPix(jj))+ii < Nx+1
%                 % place the amplitude 1 spike in the right place, with
%                 % linear interpolation if it falls in-between
%                 tmp(floor(epsilonPix(jj))+ii,kk) = ...
%                     tmp(floor(epsilonPix(jj))+ii,kk) + ceil(epsilonPix(jj)) - epsilonPix(jj);
%                 tmp(ceil(epsilonPix(jj))+ii,kk) = ...
%                     tmp(ceil(epsilonPix(jj))+ii,kk) + epsilonPix(jj) - floor(epsilonPix(jj));
%             end
        end
    
    %end
    img(:,:,ii) = tmp;
    disp(num2str(ii))
end
