%% Authored by Joshua Soneson 2007
function[] = KZK_radial_plots(r,Ir,H,p5,p0,rho,c,R,a)
% Produces the following plots:
%	Heating rate vs. radius at focus
%	Intensity vs. radius at focus
%	Pressure amplitude of first (up to 5) harmonics vs. radius at focus

R = a*R;

p5 = 1e-6*p0*p5;
figure
axes('FontSize',18)
plot(r,p5,'LineWidth',2)
ylim = get(gca,'YLim');
axis([0,R,ylim(1),ylim(2)])
xlabel('r (cm)')
ylabel('p (MPa)')
grid

Ir = 1e-4*0.5*p0*p0*Ir/rho/c;
figure
axes('FontSize',18)
plot(r,Ir,'LineWidth',2)
ylim = get(gca,'YLim');
axis([0,R,ylim(1),ylim(2)])
xlabel('r (cm)')
ylabel('I (W/cm^2)')
grid

figure
axes('FontSize',18)
plot(r,H,'r','LineWidth',2)
ylim = get(gca,'YLim');
axis([0,R,ylim(1),ylim(2)])
xlabel('r (cm)')
ylabel('H (W/cm^3)')
grid
