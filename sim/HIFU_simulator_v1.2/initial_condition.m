%% Authored by Joshua Soneson 2007, updated 2010
function[v] = initial_condition(J,K,G,r,ir,limit)
%% Determines the initial condition for the KZK equation.  In this
%% case, it's a uniform pressure distribution with a phase shift
%% corresponding to a quadratic approximation of a spherical 
%% converging wave, appropriate for above f/1.37. 

v = zeros(2*J,K);
for j=1:J
  if(abs(r(j))>=ir & abs(r(j))<=limit)
    arg = G*r(j)*r(j); 
    v(j,1) = cos(arg); 
    v(J+j,1) = -sin(arg); 
end; end
