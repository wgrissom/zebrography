%% Authored by Joshua Soneson 2007
function[] = plot_waveform(p0,f,X,z,K)
% plots a single cycle of the time-domain waveform

figure
z = 0.01*round(100*z);
axes('FontSize',18)
dt = 1/f/(K-1);
t = 1e6*[0:dt:1/f];
X = 1e-6*p0*X;
plot(t,X,'LineWidth',2)
xlabel('t (\mu s)')
ylabel('p (MPa)')
ylimits = get(gca,'YLim');
axis([t(1),t(K),ylimits(1),ylimits(2)])
title(['Waveform at z = ',num2str(z),' cm'])
grid
