%% Authored by Joshua Soneson 2007
function[p1] = timing(p1,p2,t_start,z,d)
% Determines the amount of CPU time since t_start was determined
 
if(p2>p1)
  time = etime(clock,t_start);
  h = floor(time/3600);
  time = time - h*3600;
  m = floor(time/60);
  time = time - m*60;
  p1 = p2;
  fprintf('\t\t%2.1f\t%d:%d:%2.1f\n',z*d,h,m,time);
end
  
