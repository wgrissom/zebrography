%% Authored by Joshua Soneson 2007
function[] = KZK_axial_plots(z,Ix,p5,H,peak,trough,p0,rho1,rho2,c1,c2,d,Z,a,m_)
% Produces the following plots:
%	Axial heating rate vs. propagation distance
%	Axial intensity vs. propagation distance
%	Axial pressure amplitude of the first (up to 5) harmonics vs. distance
%	peak positive and negative pressure vs. propagation distance

% Rescale quantities:
Z = d*Z;
p5 = 1e-6*p0*p5;
peak = 1e-6*p0*peak;
trough = 1e-6*p0*trough;

[K,N] = size(p5);
% peak positive and negative pressure
if(K>1)
  figure
  axes('FontSize',18)
  plot(z,peak,z,trough,'LineWidth',2)
  ylim = get(gca,'YLim');
  axis([0,Z,ylim(1),ylim(2)])
  xlabel('z (cm)')
  ylabel('p (MPa)')
  grid
end

% first 5 harmonics
figure
axes('FontSize',18)
plot(z,p5,'LineWidth',2)
ylim = get(gca,'YLim');
axis([0,Z,ylim(1),ylim(2)])
xlabel('z (cm)')
ylabel('p (MPa)')
grid

% intensity
M = length(Ix);
Ix(1:m_) = 1e-4*0.5*p0*p0*Ix(1:m_)/rho1/c1;
Ix(m_+1:M) = 1e-4*0.5*p0*p0*Ix(m_+1:M)/rho2/c2;
figure
axes('FontSize',18)
plot(z,Ix,'LineWidth',2)
ylim = get(gca,'YLim');
axis([0,Z,ylim(1),ylim(2)])
xlabel('z (cm)')
ylabel('I (W/cm^2)')
grid

% heating rate
figure
axes('FontSize',18)
plot(z,H,'r','LineWidth',2)
ylim = get(gca,'YLim');
axis([0,Z,ylim(1),ylim(2)])
xlabel('z (cm)')
ylabel('H (W/cm^3)')
grid
