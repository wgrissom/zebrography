\documentclass[11pt]{article}
% Command line: pdflatex -shell-escape compulse.tex
\usepackage{geometry} 
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{booktabs}
\usepackage{times}
\usepackage{bm}
\usepackage{fixltx2e}
\usepackage[outerbars]{changebar}
\usepackage{graphicx}
\usepackage{epstopdf}
\usepackage{color}
\usepackage{tabularx}
\usepackage{ulem}

\epstopdfsetup{suffix=} % to remove 'eps-to-pdf' suffix from converted images
%\usepackage{todonotes} % use option disable to hide all comments

\usepackage[sort&compress]{natbib}
\bibpunct{[}{]}{,}{n}{,}{,}

\usepackage[noend]{algpseudocode}

\usepackage{dsfont}
\usepackage{relsize}

%changing the Eq. tag to use [] when numbering. use \eqref{label} to reference equations in text.
\makeatletter
  \def\tagform@#1{\maketag@@@{[#1]\@@italiccorr}}
\makeatother

\linespread{1.5}
%\setlength{\parindent}{0in}

% the following command can be used to mark changes made due to reviewers' concerns. Use as \revbox{Rx.y} for reviewer x, concern y.
\newif\ifmarkedup
%\markeduptrue

\ifmarkedup
	\newcommand{\revbox}[1]{\marginpar{\framebox{\textcolor{blue}{#1}}}}
\else
	\newcommand{\revbox}[1]{}
	\renewcommand{\textcolor}[1]{}
	\renewcommand{\sout}[1]{}
\fi

%\newcommand{\bop}{$\vert B_1^+ \vert$}
\newcommand{\kt}{$k_\textrm{T}$}
\newcommand{\bmap}{$B_1^+$}
\newcommand{\mytilde}{\raise.17ex\hbox{$\scriptstyle\mathtt{\sim}$}}  % tilde symbol
\mathchardef\mhyphen="2D

\begin{document}


%%%%%%%%%%%%%%%%%%%%%%% Theory %%%%%%%%%%%%%%%%%%%%%%%

\section* {Theory}
We start with the theory in Venkatakrishnan et al. 
The image refraction angle is:
\begin{equation}
\vec{\epsilon} = \frac{1}{n_0} \int_{Z_D - \Delta Z_D}^{Z_D + \Delta Z_D} \frac{\partial n}{\partial \vec{x}} dz
\end{equation}
where $\vec{x} = (x,y)$, 
$n$ is the index of refraction field which is assumed to vary in all three spatial dimensions, and also with time,
$n_0$ is the base index of refraction of the transparent medium,
and $\vec{\epsilon} = (\epsilon_x,\epsilon_y)$.
$Z_D$ is the distance from the background to the middle of the tank, 
and the phase object is assumed to be $2 Z_D$ wide. 
The integral is taken along the line of sight through the tank to the pattern. 
An assumption here is that $\Delta Z_D \ll Z_D$. 

\par This displacement is related to the image displacement as:
\begin{equation}
\vec{\epsilon} = \frac{\Delta \vec{x}}{Z_D}.
\end{equation}
Our images will have spatial units on the background plane, 
so we directly obtain image displacements $\Delta \vec{x}$, 
and we can relate them to the index of refraction gradients using the above two 
equations. 

\par Now we have defined the background image displacement for a given index of refraction field.
In US BOS, the exposure time is much much longer than the ultrasound period,
so we cannot approximate $n$ as static in time.
A conventional static BOS acquisition would give an observed image:
\begin{equation}
I_{obs}(\vec{x}) = \int I_b(\vec{x}') \delta(\vec{x}-\vec{x}'-Z_D\vec{\epsilon}(\vec{x}')) d\vec{x}',
\end{equation}
where $I_{obs}(\vec{x})$ is the observed image, and $I_b(\vec{x})$ is the background image. 
This equation says that the observed image at a location $\vec{x}$ will be the sum of all points in the background
image that are shifted to $\vec{x}$. 
To incorporate the exposure time we extend this model to:
\begin{equation}
I_{obs}(\vec{x}) = \int_0^T \int I_b(\vec{x}') \delta(y-\vec{x}'-Z_D\epsilon(\vec{x}',t)) d\vec{x}'dt,
\end{equation}
where $T$ is the exposure time, and $\epsilon$ gains a time dependence via the dependence of $n$ on time. 
This expression looks complicated but we can simplify it by making $I_b(\vec{x})$ a delta function (i.e. a dot) centered at $\vec{x}_c$.
Then $I_b(\vec{x}) = \delta(\vec{x} - \vec{x}_c)$,
and applying the sifting property of the delta function we get:
\begin{equation}
\label{eq:deltfuncimg}
I_{obs}(\vec{x}) = \int_0^T \delta(\vec{x}-\vec{x}_c-Z_D\epsilon(\vec{x}_c,t)) dt.
\end{equation}
This equation says that we will get a non-zero intensity at $\vec{x}$ whenever the dot at $\vec{x}_c$ is deflected to $\vec{x}$.

\par So now we need to relate the pressure field to the index of refraction field. 
Index of refraction is a very linear function of pressure, so let's assume that $n = \alpha p + c$,
where $\alpha$ is a pressure-to-index of refraction conversion factor and $c$ is a constant that will drop out after differentiating $n$. 
Then the deflections are:
\begin{equation}
\label{eq:epspressure}
\vec{\epsilon} = \frac{\alpha}{n_0} \int_{Z_D - \Delta Z_D}^{Z_D + \Delta Z_D} \frac{\partial p}{\partial \vec{x}} dz,
\end{equation}
So now let's break up $p$ into its constituent parts.
Let's express it as a peak amplitude field $a(x,y,z)$, 
multiplied by an underlying propagating wave function with wavenumber $\vec{k}$:
\begin{equation}
p(x,y,z,t) = a(x,y,z) s(\omega t - \vec{k}\cdot\vec{y};x,y,z),
\end{equation}
where $\vec{y} = (x,y,z)$, $\vec{k} = (k_x,k_y,k_z)$, and $\omega$ is the 
ultrasound frequency in radians. 
The dependence on $x$, $y$, and $z$ represents phase shifts and non-linearities that may be spatially-dependent. 
Now lets assume that we are in a linear propagation regime, so that 
\begin{equation}
p(x,y,z,t) = a(x,y,z) \Re\{ e^{\imath \phi(x,y,z)} e^{\imath (\omega t - \vec{k}\cdot \vec{y})}\},
\end{equation}
and lets further plug this in to Eq. \ref{eq:epspressure}. 
Then let's assume that the $\vec{x}$-derivative of the amplitude and phase terms is much smaller than the wavenumber $\vec{k}$,
so that:
\begin{align}
\frac{\partial}{\partial \vec{x}} p &\approx a(x,y,z) \Re\{ -\imath k e^{\imath \phi(x,y,z)} e^{\imath (\omega t - \vec{k}\cdot \vec{y})}\}, \\
&= -\vec{k}a(x,y,z) \sin(\omega t - \vec{k}\cdot \vec{y} + \phi(x,y,z)).
\end{align}
If we plug this into Eq. \ref{eq:epspressure}, we will get an integral of the envelope times
the phase dispersion, and we get:
\begin{equation}
\vec{\epsilon} = -\frac{\vec{k}\alpha}{n_0} \int_{Z_D - \Delta Z_D}^{Z_D + \Delta Z_D} a(x,y,z) \sin(\omega t - \vec{k} \cdot \vec{y} + \phi(x,y,z)) dz. 
\end{equation}
Since the exposure lasts many cycles of the sine wave, 
we can neglect the $\vec{k}\cdot \vec{y}$ argument:
\begin{equation}
\vec{\epsilon} \equiv -\frac{\vec{k}\alpha}{n_0} \int_{Z_D - \Delta Z_D}^{Z_D + \Delta Z_D} a(x,y,z) \sin(\omega t  + \phi(x,y,z)) dz. 
\end{equation}
If we further neglect the phase variations through the volume, 
then we can pull out the sine function to get:
\begin{equation}
\vec{\epsilon} \equiv -\frac{\vec{k}\alpha}{n_0} \sin(\omega t) \int_{Z_D - \Delta Z_D}^{Z_D + \Delta Z_D} a(x,y,z) dz,
\end{equation}
and substituting this back into Eq. \ref{eq:deltfuncimg} we can appreciate that the 
deflections will follow a sine-wave pdf with an amplitude proportional to the integral of the 
amplitude field along the line of sight. 

\par A possibly useful fact: The pdf of a scaled sine wave $f = R \sin(\omega t)$ is:
\begin{equation}
	p(f) = \frac{1}{\pi \sqrt{ 1 - (f/R)^2 }}
\end{equation}

\par The Rayleigh equation to calculate the total pressure at a location $\vec{y}$ from a set of 
$N$ transducer elements at locations $\vec{x}_n$ is:
\begin{equation}
p(\vec{y}) = \sum_{n=1}^N \imath \rho \cos(\theta) \frac{e^{\imath k \vert \vec{x}_n - \vec{y} \vert}}{\vert \vec{x}_n - \vec{y} \vert},
\end{equation}
where $\theta$ is the angle between $\vec{x}_n - \vec{y}$ 
and the normal vector to the surface of the transducer at $\vec{x}_n$,
or equivalently the dot product of these two vectors (when both are normalized).
For our method we need to know the spatial derivatives of the pressure field, which will be dominated at 
each location by the derivative of complex exponential. 


\end{document}