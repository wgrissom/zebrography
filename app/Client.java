package com.example.masonchen.myapplication;
        import java.io.ByteArrayOutputStream;
        import java.io.IOException;
        import java.io.InputStream;
        import java.net.Socket;
        import java.net.UnknownHostException;

        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.ImageView;
        import android.os.AsyncTask;
        import android.widget.TextView;
        import android.content.Context;
/**
 * Created by masonchen on 7/15/16.
 */
public class Client extends AsyncTask<Void, Void, Void> {
    private Context context;
   /* public Client(Context context) {
        this.context = context;
    }*/

    String dstAddress;
    int dstPort;
    String response;
    TextView textResponse;
    ImageView image;

    Client(String addr, int port, TextView textResponse, ImageView image, Context context) {
        dstAddress = addr;
        dstPort = port;
        this.textResponse = textResponse;
        this.image = image;
        this.context=context;
    }

    @Override
    protected Void doInBackground(Void... arg0) {

        Socket socket = null;

        try {
            socket = new Socket(dstAddress, dstPort);

            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(
                    1024);
            byte[] buffer = new byte[1024];

            int bytesRead;
            InputStream inputStream = socket.getInputStream();

         /*
          * notice: inputStream.read() will block if no data return
          */
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                byteArrayOutputStream.write(buffer, 0, bytesRead);
                response = byteArrayOutputStream.toString("UTF-8");
                publishProgress();
                byteArrayOutputStream.reset();


            }


        } catch (UnknownHostException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            response = "UnknownHostException: " + e.toString();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            response = "IOException: " + e.toString();
            publishProgress();

        } finally {
            if (socket != null) {
                try {
                    socket.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
        return null;

    }


    protected void onProgressUpdate(Void...values) {
        textResponse.setText(response);
        if (!response.equals("connected")){
            int id=context.getResources().getIdentifier(response,"drawable",context.getPackageName());
                image.setImageResource(id);
                image.getLayoutParams().height= ViewGroup.LayoutParams.WRAP_CONTENT;
                //image.setScaleType(ImageView.ScaleType.CENTER);


        }
    }



}